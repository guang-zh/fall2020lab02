/**
 * Guang Kun Zhang
 * 1942372
 */

/**
 * @author guang
 * Java310 - Lab02 - Driver
 *
 */

public class BikeStore {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Bicycle[] objects from Bicycle class
		// Bicycle(manufacturer, numGears, maxSpeed)
		Bicycle[] bicycles = new Bicycle[4];
		bicycles[0] = new Bicycle("BigBike", 18, 50);
		bicycles[1] = new Bicycle("Specialized", 21, 40);
		bicycles[2] = new Bicycle("FunBike", 25, 30);
		bicycles[3] = new Bicycle("SpeedyBike", 50, 70);
		
		for (Bicycle i : bicycles) {
			System.out.println(i);
		}

	}

}

